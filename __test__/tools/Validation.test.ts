import { stringIsEmpty , objectIsEmpty , listIsEmpty } from "../../src/index";

test("String Is Empty : true", () => {
  const isEmpty: boolean = stringIsEmpty("");
  const Boolean: boolean = true;
  expect(isEmpty).toBe(Boolean);
});

test("String Is Empty : false", () => {
  const isEmpty: boolean = stringIsEmpty("Test");
  const Boolean: boolean = false;
  expect(isEmpty).toBe(Boolean);
});

test("Object Is Empty : true ", () => {
  const isEmpty: boolean = objectIsEmpty({});
  const Boolean: boolean = true;
  expect(isEmpty).toBe(Boolean);
});


test("Object Is Empty : false ", () => {
  const isEmpty: boolean = objectIsEmpty({ test : 'test' });
  const Boolean: boolean = false;
  expect(isEmpty).toBe(Boolean);
});


test("List Is Empty : true ", () => {
  const isEmpty: boolean = listIsEmpty([]);
  const Boolean: boolean = true;
  expect(isEmpty).toBe(Boolean);
});


test("List Is Empty : false ", () => {
  const isEmpty: boolean = listIsEmpty(['test']);
  const Boolean: boolean = false;
  expect(isEmpty).toBe(Boolean);
});
