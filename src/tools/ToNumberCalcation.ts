import { addition , Subtraction , times , division , remainder } from './Calculation'

function toNumAddition(first: number | string, second: number | string) : number {
  const firstVal : number =  typeof first === 'string' ? +first : first
  const secondVal : number =  typeof second === 'string' ? +second : second
  return addition(firstVal, secondVal);
}

function toNumSubtraction(first: number | string, second: number | string) : number {
  const firstVal : number =  typeof first === 'string' ? +first : first
  const secondVal : number =  typeof second === 'string' ? +second : second
  return Subtraction(firstVal, secondVal);
}

function toNumTimes(first: number | string, second: number | string) : number {
  const firstVal : number =  typeof first === 'string' ? +first : first
  const secondVal : number =  typeof second === 'string' ? +second : second
  return times(firstVal, secondVal);
}

function toNumDivision(first: number | string, second: number | string) : number {
  const firstVal : number =  typeof first === 'string' ? +first : first
  const secondVal : number =  typeof second === 'string' ? +second : second
  return division(firstVal, secondVal);
}

function toNumRemainder(first: number | string, second: number | string) : number {
  const firstVal : number =  typeof first === 'string' ? +first : first
  const secondVal : number =  typeof second === 'string' ? +second : second
  return remainder(firstVal, secondVal);
}

export { toNumAddition , toNumSubtraction , toNumTimes , toNumDivision , toNumRemainder }
