function stringIsEmpty(val : string) : boolean {
  const regs = "^[ ] $";
  const re = new RegExp(regs);
  if( val.length === 0 || val === null || val === undefined || re.test(val) ) {
    return true
  } else {
    return false
  }
}

function objectIsEmpty(val : object) : boolean {
  if (Object.keys(val).length === 0 || val === null || val === undefined) {
    return true;
  } else {
    return false;
  }
}

function listIsEmpty(val: any[]) : boolean {
  if( val.length === 0 ) {
    return true
  } else {
    return false
  }
}

export { stringIsEmpty , objectIsEmpty , listIsEmpty }
