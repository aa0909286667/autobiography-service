import { toNumAddition , toNumSubtraction , toNumTimes , toNumDivision , toNumRemainder } from "../../src/index";

test("It should be 12", () => {
    const total: number = toNumAddition("10", 2);
    const expected: number = 12;
    expect(total).toBe(expected);
});

test("It should be 5", () => {
    const total: number = toNumSubtraction("10", "5");
    const expected: number = 5;
    expect(total).toBe(expected);
});

test("It should be 200", () => {
    const total: number = toNumTimes(10, "20");
    const expected: number = 200;
    expect(total).toBe(expected);
});

test("It should be 50", () => {
    const total: number = toNumDivision("100", 2);
    const expected: number = 50;
    expect(total).toBe(expected);
});

test("It should be 2", () => {
    const total: number = toNumRemainder("11", 3);
    const expected: number = 2;
    expect(total).toBe(expected);
});
