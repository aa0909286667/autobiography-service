import "../../src/index";

test("Date ", () => {
    const date: string = new Date('Fri Dec 24 2021 09:48:39 GMT+0800').toFormat();
    const format: string = '2021-12-24 09:48:39';
    expect(date).toBe(format);
});

test("Date ", () => {
    const date: string = new Date('Fri Dec 24 2021 09:48:39 GMT+0800').toFormat('yyyy/MM/dd hh:mm:ss');
    const format: string = '2021/12/24 09:48:39';
    expect(date).toBe(format);
});
