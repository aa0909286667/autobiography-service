function addition(first: number, second: number): number {
    const total: number = first + second;
    return total;
}

function Subtraction(first: number, second: number): number {
    const total: number = first - second;
    return total;
}

function times(first: number, second: number): number {
    const total: number = first * second;
    return total;
}

function division(first: number, second: number): number {
    const total: number = first / second;
    return total;
}

function remainder(first: number, second: number): number {
    const total: number = first % second;
    return total;
}

export { addition , Subtraction , times , division , remainder }
