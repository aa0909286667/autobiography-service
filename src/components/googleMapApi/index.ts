import { Loader } from "@googlemaps/js-api-loader"

let _loader : Loader
let _map: google.maps.Map;


// loader 參數
function loader(_apiKey: string) : void {
  _loader = new Loader({
    apiKey: _apiKey,
    version: "weekly",
  });
}

// 初始化Google Map
function initMap(_coordinate : { lat: number , lng: number }, _zoon : number): void {
  // 炫染Google Map
  const center :　google.maps.LatLngLiteral = { lat: _coordinate.lat, lng: _coordinate.lng };
  _loader.load().then(() => {
    _map = new google.maps.Map( document.getElementById("map") as HTMLElement, {
      center,
      zoom: _zoon
    });

  })
}

// 設置Google Map Marker
function setMarker(_position : { lat: number , lng: number }, _content? :  string | Element | Text, _maxWidth? : number) {
  _loader.load().then(() => {
    const marker = new google.maps.Marker({
      position : { lat: _position.lat, lng: _position.lng },
      map: _map
    })
    if (_content !== null && _content !== undefined) {
      addListener(marker, _content, _maxWidth)
    }
  })
}

function addListener ( marker : google.maps.Marker, _content? :  string | Element | Text, _maxWidth? : number ) {
  const infoWindow = new google.maps.InfoWindow({
    content: _content,
    maxWidth: _maxWidth
  });
  marker.addListener("click", () =>{
    infoWindow.open(_map , marker)
  })
}

export { loader , initMap , setMarker }
