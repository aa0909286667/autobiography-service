import { addition , Subtraction , times , division , remainder } from "../../src/index";

test("It should be 3", () => {
    const total: number = addition(1, 2);
    const expected: number = 3;
    expect(total).toBe(expected);
});

test("It should be 5", () => {
    const total: number = Subtraction(10, 5);
    const expected: number = 5;
    expect(total).toBe(expected);
});

test("It should be 100", () => {
    const total: number = times(10, 10);
    const expected: number = 100;
    expect(total).toBe(expected);
});

test("It should be 2", () => {
    const total: number = division(10, 5);
    const expected: number = 2;
    expect(total).toBe(expected);
});

test("It should be 5", () => {
    const total: number = remainder(10, 3);
    const expected: number = 1;
    expect(total).toBe(expected);
});
