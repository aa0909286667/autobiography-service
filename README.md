# autobiography-service

### 因應autobiography會拆分"PC版"及"mobile版"而共同Function為了避免重複撰寫,也好方便測試化而產生

#### Install:
```
yarn install
```

#### Run:
```
yarn dev
```

#### Testing:
```
yarn test
```

#### Build:
```
yarn build
```
