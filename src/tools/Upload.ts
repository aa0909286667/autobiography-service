let target : HTMLInputElement;
let file : File;


interface File{
  lastModified : number,
  name : string,
  size : number,
  type : string,
}

// upload 初始化參數
let  upload = function(event: { target: HTMLInputElement; }) : void {
  target = event?.target as HTMLInputElement;
  file = (target.files as FileList)[0];
}

interface upload {
  accept ( acceptExtensionName : string | string[] ) : boolean
  showImage () : string
  getFilesName() : string
  getFilesExtension() : string
}
/******************************************************共用********************************************************/
// 檢核檔案是否存
function fileIsUndefined() : boolean {
  if( file === undefined ) {
    return true
  } else {
    return false
  }
}

// 取得檔案名
upload.prototype.getFilesName = function() : string {
  if( fileIsUndefined() ) console.error('No files found')
  return file?.name
}

// 取得檔案副檔名
upload.prototype.getFilesExtension = function() : string {
  if( fileIsUndefined() ) console.error('No files found')
  return file?.name.slice((file?.name.lastIndexOf(".") - 1 >>> 0) + 2);
}

// 限制上傳檔案
upload.prototype.accept = function(acceptExtensionName : string | string[]): boolean {
  // 檢核值
  let flag = false;
  // 取得副檔名
  const fileExtension = file?.name.slice((file?.name.lastIndexOf(".") - 1 >>> 0) + 2);

  // 檢核單字串副檔名
  if(typeof acceptExtensionName === "string" ) {
    if ( acceptExtensionName === fileExtension ) {
      flag = true;
    }
    // 檢核陣列副檔名
  } else {
    acceptExtensionName.map( name => {
      if (name === fileExtension) {
        flag = true;
      }
    })
  }
  return flag
}
/******************************************************IMAGE********************************************************/
upload.prototype.showImage =  function () : string {
  const reader : FileReader = new FileReader();
  return new Promise((resolve) => {
    const reader = new FileReader();
    reader.onloadend = () => resolve(reader.result)
    reader.readAsDataURL(file as unknown as Blob);
  }) as unknown as string
}
/********************************************************TXT**********************************************************/


export { upload }
